def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple, to_change_to: tuple) -> list[list[tuple[int, int, int]]]:
    for i in range(len(image)):
        for j in range(len(image[i])):
            if image[i][j] == to_change:
                image[i][j] = to_change_to
    return image

def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    rotated_image = []

    for i in range(cols):
        rotated_image.append([])
        for j in range(rows):
            rotated_image[i].append(image[j][-i])
    return rotated_image

def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    image.reverse()

    return image

def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    rotated_colors = []

    for i in range(rows):

        rotated_colors.append([])

        for j in range(cols):

            r, g, b = image[i][j]

            r = (r + increment) % 256
            g = (g + increment) % 256
            b = (b + increment) % 256

            if increment >= 0:
                rotated_colors[i].append((r, g, b))

            else:
                rotated_colors[i].append((r, g, b))
    return rotated_colors

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    blured_image = []
    for i in range(rows):

        blured_image.append([])

        for j in range(cols):

            contador = 0

            if i == 0:

                r, g, b = image[i+1][j]

                contador += 1

            elif i+1 == rows:

                r, g, b = image[i-1][j]
                contador += 1

            else:
                r, g, b = image[i+1][j][0] + image[i-1][j][0], image[i+1][j][1] + image[i-1][j][1], image[i+1][j][2] + image[i-1][j][2]
                contador += 2

            if j == 0:

                r, g, b = r + image[i][j + 1][0], g + image[i][j + 1][1], b + image[i][j + 1][2]
                contador += 1

            elif j+1 == cols:

                r, g, b = r + image[i][j - 1][0], g + image[i][j - 1][1], b + image[i][j - 1][2]
                contador += 1

            else:
                r, g, b = r + image[i][j + 1][0] + image[i][j - 1][0], g + image[i][j + 1][1] + image[i][j - 1][1], b + image[i][j + 1][2] + image[i][j - 1][2]
                contador += 2

            r, g, b = r // contador, g // contador, b // contador

            blured_image[i].append((r, g, b))

    return blured_image

def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    cols = len(image)
    rows = len(image[0])

    shifted_image = []
    shifted_image1 = []
    for i in range(cols):
        shifted_image.append([])
        for j in range(rows):

            if i < horizontal or i >= cols + horizontal:

                shifted_image[i].append((0, 0, 0))

            else:

                shifted_image[i].append(image[i-horizontal][j])

    for i in range(cols):
        shifted_image1.append([])
        for j in range(rows):

            if j >= rows - vertical or j < - vertical:
                shifted_image1[i].append((0, 0, 0))

            else:
                shifted_image1[i].append(shifted_image[i][j + vertical])

    return shifted_image1

def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    x_end = min(x + width, cols)
    y_end = min(y + height, rows)

    cropped_image = []

    for i in range(y, y_end):
        if 0 <= i < rows:
            cropped_image.append([])
            for j in range(x, x_end):
                if 0 <= j < cols:
                    cropped_image[i - y].append(image[i][j])

    return cropped_image

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    grayscale_image = []

    for i in range(rows):
        grayscale_image.append([])
        for j in range(cols):

            gray_value = sum(image[i][j]) // 3
            grayscale_image[i].append((gray_value, gray_value, gray_value))

    return grayscale_image

def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:

    rows = len(image)
    cols = len(image[0])

    filtered_image = []

    for i in range(rows):
        filtered_image.append([])
        for j in range(cols):

            new_red = min(int(image[i][j][0] * r), 255)
            new_green = min(int(image[i][j][1] * g), 255)
            new_blue = min(int(image[i][j][2] * b), 255)

            filtered_image[i].append((new_red, new_green, new_blue))

    return filtered_image

def filter_sepia(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])

    for i in range(rows):
        for j in range(cols):
            old_r, old_g, old_b = image[i][j]

            new_r = int((old_r * r) + (old_g * g) + (old_b * b))
            new_g = int((old_r * 0.349) + (old_g * 0.686) + (old_b * 0.168))
            new_b = int((old_r * 0.272) + (old_g * 0.534) + (old_b * 0.131))

            new_r = max(0, min(new_r, 255))
            new_g = max(0, min(new_g, 255))
            new_b = max(0, min(new_b, 255))

            image[i][j] = (new_r, new_g, new_b)

    return image
