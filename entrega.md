# ENTREGA CONVOCATORIA ENERO

Eva López Sánchez
- Correo electrónico: e.lopezs.2022@alumnos.urjc.es

https://youtu.be/uyBI51-3-Y4

Requisitos mínimos:
- change_colors
- rotate_right
- mirror
- rotate_colors
- blur
- shift
- crop
- grayscale
- filter
- transform_simple.py
- transform_args.py
- transform_multi.py

Requisitos opcionales:
- Filtros avanzados: filter_sepia